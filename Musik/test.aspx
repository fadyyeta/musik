﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="Musik.test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="js/audioPlayer.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <audio id="playerbox" muted="Muted" autoplay="autoplay" src="Audio/1.mp3"></audio>

        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#playerbox")[0].play();
                $("#playerbox").prop('muted', false);
            });
        </script>
    </form>
</body>
</html>

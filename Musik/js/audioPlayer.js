﻿$(document).ready(function () {

    var el = $('#playerbox');
    var audioSources = [];
    //$('#playerbox')[0].volume = 0.5;

    var index = parseInt($("#playIndex").val());

    function loadList() {
        var dataSrc;
        var length = $('#playlistUl li').length;
        audioSources = [];
        var listItems = $("#playlistUl li");
        listItems.each(function (index) {
            var src = $(this).find("[href^='Audio']").attr("href");
            dataSrc = src;
            audioSources.push(dataSrc);


        });

    }

    function playNow(indexNow) {
        $("#playerbox")[0].pause();
        $("#jp_container").addClass("jp-video-270p jp-state-playing");
        $("audio").attr('src', audioSources[indexNow]);
        $("#playerbox")[0].play();
        $("#playerbox").prop('muted', false);

    }
    loadList();
    playNow(index);

    function playNext(indexNext) {
        $("#playerbox")[0].pause();
        if (($(".jp-shuffle").hasClass("hid"))) {
            Shuffle(audioSources);
        }
        $("#playIndex").attr('value', indexNext);
        if (indexNext < audioSources.length) {
            $('#playerbox').attr('src', audioSources[indexNext]);
            $("#playerbox")[0].play();

        }
    }

    $("#nextP").click(function () {
        $("#playerbox")[0].pause();
        var index = parseInt($("#playIndex").val());
        if (index < audioSources.length - 1) {
            $("#playerbox")[0].pause();

            index++;
            $("#playerbox")[0].pause();
            playNext(index)
        }

        if (($(".jp-shuffle").hasClass("hid"))) {
            $("#playerbox")[0].pause();

            Shuffle(audioSources);
            playNext(index);
        }

    });
    $("#prevP").click(function () {
        var index = parseInt($("#playIndex").val());
        if (index > 0) {
            $("#playerbox")[0].pause();
            index--;
            $("#playerbox")[0].pause();
            playNext(index);
        }
        if (($(".jp-shuffle").hasClass("hid"))) {
            $("#playerbox")[0].pause();
            Shuffle(audioSources);
            playNext(index);
        }
    });

    setInterval(function () {
        var y = $("#playerbox")[0].paused;
        if (y == true) {
            //alert("next");

            $(".jp-pause").addClass("hid");
            $(".jp-play").removeClass("hid");

        }
        else {

            $(".jp-play").addClass("hid");
            $(".jp-pause").removeClass("hid");
        }
        //var hrefThis = $($(this).siblings("[href^='Audio']")).attr("href");

        //
        currentDur();
    }, 3);
    function currentDur() {
        var audioElement = $("#playerbox")[0];
        var curT = audioElement.currentTime;
        var durationX = audioElement.duration;
        var perc = (curT / durationX) * 100;
        //var durMS = (durationX - curT) * 1000;

        //if (perc <= 100) {
        //    var CTX = (parseInt(perc) / 100) * durationX;
        //}
        //alert(CTX);
        //setCurTime(CTX);
        $(".jp-play-bar .ui-slider-range.ui-corner-all.ui-widget-header.ui-slider-range-min").css("width", (perc) + "%");

    }
    function Shuffle(o) {
        for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
        //alert(o);
        return o;
    };
    //play next audio after this is finished
    setInterval(function () {

        var xEnded = $("#playerbox")[0].ended;
        //alert(x);
        var repeatedEnd = $(".jp-repeat").hasClass("hid");
        var yEnded = parseInt($("#playIndex").val());
        if (xEnded == true) {
            //alert(repeatedEnd);
            //alert(yEnded);
            if (yEnded < (audioSources.length -1)) {
                $("#playIndex").attr("value", yEnded);
                playNext(yEnded + 1);
            }
            if ((yEnded == (audioSources.length - 1)) && (repeatedEnd == true)) {
                $("#playIndex").attr("value",0);
                playNext(0);
            }
            if ((yEnded == (audioSources.length - 1)) && (repeatedEnd == false)) {
                $("#playerbox")[0].pause;
            }

        }
        $(".jp-playlist-current").removeClass("jp-playlist-current");
        var currentHref = $("audio").attr("src");
        var isPlayedNow = $("[href^='" + currentHref + "']").siblings(".jp-playlist-item");
        $(isPlayedNow).addClass("jp-playlist-current");

    }, 300);

    $('#player').click(function () {

        var clicks = $(this).data('clicks');
        if (clicks) {
            // odd clicks  
            //$('header .aside').css('width', '70px');
            $("#playerbox")[0].play();
            $(".jp-play").addClass("hid");
            $(".jp-pause").removeClass("hid");

        } else {
            // even clicks
            $("#playerbox")[0].pause();
            $(".jp-pause").addClass("hid");
            $(".jp-play").removeClass("hid");

        }
        $(this).data("clicks", !clicks);
    });
    $('#repeatP').click(function () {
        var clicks = $(this).data('clicks');
        if (clicks) {
            // odd clicks  
            //$('header .aside').css('width', '70px');
            $(".jp-repeat-off").addClass("hid");
            $(".jp-repeat").removeClass("hid");
            $("#repeatP").removeClass("repeatP");

        } else {
            // even clicks
            $("#repeatP").addClass("repeatP");
            $(".jp-repeat").addClass("hid");
            $(".jp-repeat-off").removeClass("hid");

        }
        $(this).data("clicks", !clicks);
    });
    $('#shuffleP').click(function () {
        var clicks = $(this).data('clicks');
        if (clicks) {
            // odd clicks  
            //$('header .aside').css('width', '70px');
            $(".jp-shuffle-off").addClass("hid");
            $(".jp-shuffle").removeClass("hid");
            $("#shuffleP").removeClass("shuffleP");

        } else {
            // even clicks
            $("#shuffleP").addClass("shuffleP");
            $(".jp-shuffle").addClass("hid");
            $(".jp-shuffle-off").removeClass("hid");

        }
        $(this).data("clicks", !clicks);
        //});
        //$(".jp-volume-bar-value.lter").slider({
        //    value: 75,
        //    step: 1,
        //    range: 'min',
        //    min: 0,
        //    max: 100,
        //    change: function () {
        //        var value = $(".jp-volume-bar-value.lter").slider("value");
        //        $("#playerbox")[0].volume = (value / 100);
        //    }
        //});

    });
    $('#muteP').click(function () {
        var clicks = $(this).data('clicks');
        if (clicks) {
            // odd clicks  
            $(".jp-unmute").addClass("hid");
            $(".jp-mute").removeClass("hid");
            $("#muteP").removeClass("muteP");
            if (volChan == null) {
                volChan = 1;
            }
            setVolume(volChan);
            $(".jp-volume").css("opacity", 1);

        }
        else {
            // even clicks
            $("#muteP").addClass("muteP");
            $(".jp-mute").addClass("hid");
            $(".jp-unmute").removeClass("hid");
            setVolume(0);
            $(".jp-volume").css("opacity", 0.5);
        }
        $(this).data("clicks", !clicks);

    });

    var volChan;
    $(".jp-playlist-item").click(function () {
        $("#playerbox")[0].pause();
        $("#jp_container").addClass("jp-video-270p jp-state-playing");
        var hrefThis = $($(this).siblings("[href^='Audio']")).attr("href");
        $("audio").attr('src', hrefThis);
        var indexThis = jQuery.inArray(hrefThis, audioSources);
        $("#playIndex").attr('value', indexThis);
        $("#playerbox")[0].play();
        $("#playerbox").prop('muted', false);
    });
    $(".jp-playlist-item-remove").click(function () {
        var hrefThis = $($(this).siblings("[href^='Audio']")).attr("href");
        var index = parseInt($("#playIndex").val());
        if (hrefThis == $("audio").attr('src')) {
            playNext(index + 1);
        }

        $(this).closest(".list-group-item").remove();
        loadList();
    });
    // start volume options
    $("#slider").slider({
        min: 0,
        max: 100,
        value: 100,
        range: "min",
        slide: function (event, ui) {
            volChan = ui.value / 100;
            if (($(".jp-unmute").hasClass("hid"))) {
                setVolume(ui.value / 100);
            }

        }
    });
    function setVolume(myVolume) {
        var myMedia = $('#playerbox')[0];
        myMedia.volume = myVolume;
    }
    $("#waveform").slider({
        min: 0,
        max: 100,
        value: 0,
        range: "min",
        slide: function (event, ui) {
            var myMedia = $('#playerbox')[0];
            var progressValue = jQuery(this).val();
            myMedia.sound.setPosition(progressValue);
        }
    });
    var audioElement = $("#playerbox")[0];
    audioElement.addEventListener("loadedmetadata", function (_event) {
        var durationX = audioElement.duration;
        //alert(duration);
        //TODO whatever
    });

    function setCurTime(currentTimeX) {
        audioElement.currentTime = currentTimeX;
    }
    $("#waveform").click(function (e) {
        var perc = e.offsetX / $(this).width() * 100;
        var durationX = audioElement.duration;

        if (perc <= 100) {
            var CTX = (parseInt(perc) / 100) * durationX;
        }
        //alert(CTX);
        setCurTime(CTX);
        $(this).children(".jp-play-bar .ui-slider-range.ui-corner-all.ui-widget-header.ui-slider-range-min").css("width", perc + "%");
    });
});



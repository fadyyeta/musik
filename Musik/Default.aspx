﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Musik.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Home Page
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHeader" runat="server">
    <style>
        #contentLoad {
            overflow:hidden
        }
    </style>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <section class="mainContent">
        <div id="contentLoad"></div>
    </section>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">

    <script>
        $(document).ready(
        function loadContent() {
            $("#contentLoad").load("loadPage.aspx#ajax-container");
        });
    </script>
</asp:Content>

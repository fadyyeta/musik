﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="loadPage.aspx.cs" Inherits="Musik.loadPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="formLoaded" runat="server" >
        <div>
            <section class="w-f-md" id="ajax-container">
                <section class="hbox stretch">
                    <section>
                        <section class="vbox">
                            <section class="scrollable wrapper-lg">
                                <div id="music_post_widget-1" class="widget-1 widget-odd widget_music_post_widget widget">
                                    <h2 class="font-thin m-b m-t-none">What&#8217;s New</h2>
                                    <div class="row row-sm">
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-18" class="item post-18 download type-download status-publish has-post-thumbnail hentry download_category-reggae download_category-rnb download_category-rock download_category-soul download_tag-dance download_artist-miaow odd edd-download edd-download-cat-reggae edd-download-cat-rnb edd-download-cat-rock edd-download-cat-soul edd-download-tag-dance">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="18" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <form id="edd_purchase_18" class="edd_download_purchase_form edd_purchase_18" method="post">


                                                                <div class="edd_purchase_submit_wrapper">
                                                                    <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="18" data-variable-price="no" data-price-mode="single" data-price="0.99"><span class="edd-add-to-cart-label">&#36;0.99</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                    <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="&#036;0.99" data-action="edd_add_to_cart" data-download-id="18" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                    <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                        <span class="edd-cart-added-alert" style="display: none;">
                                                                            <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                    </span>
                                                                </div>
                                                                <!--end .edd_purchase_submit_wrapper-->

                                                                <input type="hidden" name="download_id" value="18">
                                                                <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                            </form>
                                                            <!--end #edd_purchase_18-->
                                                        </div>
                                                    </div>
                                                    <a href="music/tempered-song/index.html" data-ajax title="Tempered song">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/m19-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m19-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m19.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/tempered-song/index.html" data-ajax title="Tempered song" class="text-ellipsis">Tempered song</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/miaow/index.html" rel="tag">Miaow</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-593" class="item post-593 download type-download status-publish has-post-thumbnail hentry download_category-jazz download_tag-station even edd-download edd-download-cat-jazz edd-download-tag-station">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="593" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <a href="wp-admin/abc-jazzbf42?action=download&amp;file=http://listen.radionomy.com/abc-jazz" class="item-download pull-right">
                                                                <i class="icon-arrow-down text-white"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <a href="music/radio-station/index.html" data-ajax title="Radio Station">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/b19-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b19-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b19.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/radio-station/index.html" data-ajax title="Radio Station" class="text-ellipsis">Radio Station</a>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-2180" class="item post-2180 download type-download status-publish has-post-thumbnail hentry download_category-acoustic download_artist-trevorgoodrum odd edd-download edd-download-cat-acoustic">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="2180" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <form id="edd_purchase_2180" class="edd_download_purchase_form edd_purchase_2180" method="post">


                                                                <div class="edd_purchase_submit_wrapper">
                                                                    <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="2180" data-variable-price="no" data-price-mode="single" data-price="0.00"><span class="edd-add-to-cart-label">Free</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                    <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="Free" data-action="edd_add_to_cart" data-download-id="2180" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                    <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                        <span class="edd-cart-added-alert" style="display: none;">
                                                                            <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                    </span>
                                                                </div>
                                                                <!--end .edd_purchase_submit_wrapper-->

                                                                <input type="hidden" name="download_id" value="2180">
                                                                <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                            </form>
                                                            <!--end #edd_purchase_2180-->
                                                        </div>
                                                    </div>
                                                    <a href="music/the-voice-inspiring-emotional-blind-auditions/index.html" data-ajax title="The Voice &#8211; Inspiring &#038; Emotional Blind Auditions">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/b14-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b14-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b14.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/the-voice-inspiring-emotional-blind-auditions/index.html" data-ajax title="The Voice &#8211; Inspiring &#038; Emotional Blind Auditions" class="text-ellipsis">The Voice &#8211; Inspiring &#038; Emotional Blind Auditions</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/trevorgoodrum/index.html" rel="tag">Trevorgoodrum</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-1425" class="item post-1425 download type-download status-publish has-post-thumbnail hentry download_category-folk download_tag-radio even edd-download edd-download-cat-folk edd-download-tag-radio">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="1425" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <form id="edd_purchase_1425" class="edd_download_purchase_form edd_purchase_1425" method="post">


                                                                <div class="edd_purchase_submit_wrapper">
                                                                    <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="1425" data-variable-price="no" data-price-mode="single" data-price="0.00"><span class="edd-add-to-cart-label">Free</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                    <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="Free" data-action="edd_add_to_cart" data-download-id="1425" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                    <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                        <span class="edd-cart-added-alert" style="display: none;">
                                                                            <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                    </span>
                                                                </div>
                                                                <!--end .edd_purchase_submit_wrapper-->

                                                                <input type="hidden" name="download_id" value="1425">
                                                                <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                            </form>
                                                            <!--end #edd_purchase_1425-->
                                                        </div>
                                                    </div>
                                                    <a href="music/shoutcast/index.html" data-ajax title="SHOUTcast">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/photo-1422022098106-b3a9edc463af-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/photo-1422022098106-b3a9edc463af-150x150.jpeg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/photo-1422022098106-b3a9edc463af-350x350.jpeg 350w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/shoutcast/index.html" data-ajax title="SHOUTcast" class="text-ellipsis">SHOUTcast</a>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-1004" class="item post-1004 download type-download status-publish has-post-thumbnail hentry download_category-country download_artist-trevorgoodrum odd edd-download edd-download-cat-country">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="1004" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <form id="edd_purchase_1004" class="edd_download_purchase_form edd_purchase_1004" method="post">


                                                                <div class="edd_purchase_submit_wrapper">
                                                                    <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="1004" data-variable-price="no" data-price-mode="single" data-price="0.00"><span class="edd-add-to-cart-label">Free</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                    <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="Free" data-action="edd_add_to_cart" data-download-id="1004" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                    <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                        <span class="edd-cart-added-alert" style="display: none;">
                                                                            <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                    </span>
                                                                </div>
                                                                <!--end .edd_purchase_submit_wrapper-->

                                                                <input type="hidden" name="download_id" value="1004">
                                                                <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                            </form>
                                                            <!--end #edd_purchase_1004-->
                                                        </div>
                                                    </div>
                                                    <a href="music/country-girl-shake-it-for-me/index.html" data-ajax title="Country girl shake it for me">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/m0-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m0-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m0.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/country-girl-shake-it-for-me/index.html" data-ajax title="Country girl shake it for me" class="text-ellipsis">Country girl shake it for me</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/trevorgoodrum/index.html" rel="tag">Trevorgoodrum</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-237" class="item post-237 download type-download status-publish has-post-thumbnail hentry download_category-acoustic download_category-ambient download_artist-the-stark-palace even edd-download edd-download-cat-acoustic edd-download-cat-ambient">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="237" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <form id="edd_purchase_237" class="edd_download_purchase_form edd_purchase_237" method="post">


                                                                <div class="edd_purchase_submit_wrapper">
                                                                    <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="237" data-variable-price="no" data-price-mode="single" data-price="0.99"><span class="edd-add-to-cart-label">&#36;0.99</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                    <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="&#036;0.99" data-action="edd_add_to_cart" data-download-id="237" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                    <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                        <span class="edd-cart-added-alert" style="display: none;">
                                                                            <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                    </span>
                                                                </div>
                                                                <!--end .edd_purchase_submit_wrapper-->

                                                                <input type="hidden" name="download_id" value="237">
                                                                <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                            </form>
                                                            <!--end #edd_purchase_237-->
                                                        </div>
                                                    </div>
                                                    <a href="music/cro-magnon-man/index.html" data-ajax title="Cro Magnon Man">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/b18-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b18-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b18.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/cro-magnon-man/index.html" data-ajax title="Cro Magnon Man" class="text-ellipsis">Cro Magnon Man</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/the-stark-palace/index.html" rel="tag">The Stark Palace</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-118" class="item post-118 download type-download status-publish has-post-thumbnail hentry download_category-metal download_category-pop download_category-pop-punk download_category-punk download_artist-miaow odd edd-download edd-download-cat-metal edd-download-cat-pop edd-download-cat-pop-punk edd-download-cat-punk">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="118" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <form id="edd_purchase_118" class="edd_download_purchase_form edd_purchase_118" method="post">


                                                                <div class="edd_purchase_submit_wrapper">
                                                                    <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="118" data-variable-price="no" data-price-mode="single" data-price="0.99"><span class="edd-add-to-cart-label">&#36;0.99</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                    <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="&#036;0.99" data-action="edd_add_to_cart" data-download-id="118" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                    <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                        <span class="edd-cart-added-alert" style="display: none;">
                                                                            <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                    </span>
                                                                </div>
                                                                <!--end .edd_purchase_submit_wrapper-->

                                                                <input type="hidden" name="download_id" value="118">
                                                                <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                            </form>
                                                            <!--end #edd_purchase_118-->
                                                        </div>
                                                    </div>
                                                    <a href="music/hidden/index.html" data-ajax title="Hidden">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/b15-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b15-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b15.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/hidden/index.html" data-ajax title="Hidden" class="text-ellipsis">Hidden</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/miaow/index.html" rel="tag">Miaow</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-230" class="item post-230 download type-download status-publish has-post-thumbnail hentry download_category-acoustic download_category-blues download_artist-miaow even edd-download edd-download-cat-acoustic edd-download-cat-blues">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="230" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <form id="edd_purchase_230" class="edd_download_purchase_form edd_purchase_230" method="post">


                                                                <div class="edd_purchase_submit_wrapper">
                                                                    <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="230" data-variable-price="no" data-price-mode="single" data-price="0.99"><span class="edd-add-to-cart-label">&#36;0.99</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                    <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="&#036;0.99" data-action="edd_add_to_cart" data-download-id="230" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                    <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                        <span class="edd-cart-added-alert" style="display: none;">
                                                                            <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                    </span>
                                                                </div>
                                                                <!--end .edd_purchase_submit_wrapper-->

                                                                <input type="hidden" name="download_id" value="230">
                                                                <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                            </form>
                                                            <!--end #edd_purchase_230-->
                                                        </div>
                                                    </div>
                                                    <a href="music/the-ice/index.html" data-ajax title="The ice">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/m12-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m12-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m12.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/the-ice/index.html" data-ajax title="The ice" class="text-ellipsis">The ice</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/miaow/index.html" rel="tag">Miaow</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-632" class="item post-632 download type-download status-publish has-post-thumbnail hentry download_category-blues download_tag-soundcloud download_artist-bossr odd edd-download edd-download-cat-blues edd-download-tag-soundcloud">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="632" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                        </div>
                                                    </div>
                                                    <a href="music/soundcloud/index.html" data-ajax title="Soundcloud">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/b1-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b1-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b1.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/soundcloud/index.html" data-ajax title="Soundcloud" class="text-ellipsis">Soundcloud</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/bossr/index.html" rel="tag">Bossr</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-181" class="item post-181 download type-download status-publish has-post-thumbnail hentry download_category-acoustic download_category-ambient download_category-blues download_artist-miaow even edd-download edd-download-cat-acoustic edd-download-cat-ambient edd-download-cat-blues">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="181" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <form id="edd_purchase_181" class="edd_download_purchase_form edd_purchase_181" method="post">


                                                                <div class="edd_purchase_submit_wrapper">
                                                                    <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="181" data-variable-price="no" data-price-mode="single" data-price="0.99"><span class="edd-add-to-cart-label">&#36;0.99</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                    <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="&#036;0.99" data-action="edd_add_to_cart" data-download-id="181" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                    <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                        <span class="edd-cart-added-alert" style="display: none;">
                                                                            <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                    </span>
                                                                </div>
                                                                <!--end .edd_purchase_submit_wrapper-->

                                                                <input type="hidden" name="download_id" value="181">
                                                                <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                            </form>
                                                            <!--end #edd_purchase_181-->
                                                        </div>
                                                    </div>
                                                    <a href="music/lismore/index.html" data-ajax title="Lismore">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/b17-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b17-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b17.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/lismore/index.html" data-ajax title="Lismore" class="text-ellipsis">Lismore</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/miaow/index.html" rel="tag">Miaow</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-226" class="item post-226 download type-download status-publish has-post-thumbnail hentry download_category-acoustic download_artist-miaow odd edd-download edd-download-cat-acoustic">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="226" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <form id="edd_purchase_226" class="edd_download_purchase_form edd_purchase_226" method="post">


                                                                <div class="edd_purchase_submit_wrapper">
                                                                    <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="226" data-variable-price="no" data-price-mode="single" data-price="0.99"><span class="edd-add-to-cart-label">&#36;0.99</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                    <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="&#036;0.99" data-action="edd_add_to_cart" data-download-id="226" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                    <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                        <span class="edd-cart-added-alert" style="display: none;">
                                                                            <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                    </span>
                                                                </div>
                                                                <!--end .edd_purchase_submit_wrapper-->

                                                                <input type="hidden" name="download_id" value="226">
                                                                <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                            </form>
                                                            <!--end #edd_purchase_226-->
                                                        </div>
                                                    </div>
                                                    <a href="music/stirring-of-a-fool/index.html" data-ajax title="Stirring of a fool">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/b7-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b7-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b7.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/stirring-of-a-fool/index.html" data-ajax title="Stirring of a fool" class="text-ellipsis">Stirring of a fool</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/miaow/index.html" rel="tag">Miaow</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-232" class="item post-232 download type-download status-publish has-post-thumbnail hentry download_category-acoustic download_artist-the-stark-palace even edd-download edd-download-cat-acoustic">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="232" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <form id="edd_purchase_232" class="edd_download_purchase_form edd_purchase_232" method="post">


                                                                <div class="edd_purchase_submit_wrapper">
                                                                    <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="232" data-variable-price="no" data-price-mode="single" data-price="0.99"><span class="edd-add-to-cart-label">&#36;0.99</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                    <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="&#036;0.99" data-action="edd_add_to_cart" data-download-id="232" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                    <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                        <span class="edd-cart-added-alert" style="display: none;">
                                                                            <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                    </span>
                                                                </div>
                                                                <!--end .edd_purchase_submit_wrapper-->

                                                                <input type="hidden" name="download_id" value="232">
                                                                <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                            </form>
                                                            <!--end #edd_purchase_232-->
                                                        </div>
                                                    </div>
                                                    <a href="music/cyber-sonnet/index.html" data-ajax title="Cyber Sonnet">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/b19-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b19-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b19.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/cyber-sonnet/index.html" data-ajax title="Cyber Sonnet" class="text-ellipsis">Cyber Sonnet</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/the-stark-palace/index.html" rel="tag">The Stark Palace</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                                <div id="text-1" class="widget-2 widget-even widget-alt widget_text widget">
                                    <div class="textwidget">
                                        <div class="row m-t-lg m-b-lg">
                                            <div class="col-sm-6 ourLogin">
                                                <div class="bg-white wrapper-md r">
                                                    <a href="#">
                                                        <span class="h4 m-b-xs block"><i class=" icon-user-follow m-r-sm"></i>Login or Create account</span>
                                                        <span class="text-muted">Get free musics when you logged in, and give your comments to them.</span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 ourApp">
                                                <div class="bg-info dker wrapper-md r">
                                                    <a href="#">
                                                        <span class="h4 m-b-xs block"><i class="icon-cloud-download  m-r-sm"></i>Download our app</span>
                                                        <span class="text-muted">Get the apps for desktop and mobile to start listening music at anywhere and anytime.</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="music_post_widget-2" class="widget-3 widget-odd widget_music_post_widget widget">
                                    <h2 class="font-thin m-b m-t-none">New Albums</h2>
                                    <div class="row row-sm">
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-417" class="item post-417 download type-download status-publish has-post-thumbnail hentry download_category-acoustic download_artist-bossr odd edd-download edd-download-cat-acoustic">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="417" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                        </div>
                                                    </div>
                                                    <a href="music/top-board-june/index.html" data-ajax title="Top Board &#8211; June">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/m13-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m13-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m13.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/top-board-june/index.html" data-ajax title="Top Board &#8211; June" class="text-ellipsis">Top Board &#8211; June</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/bossr/index.html" rel="tag">Bossr</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-415" class="item post-415 download type-download status-publish has-post-thumbnail hentry download_category-country download_artist-miaow even edd-download edd-download-cat-country">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="415" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <form id="edd_purchase_415" class="edd_download_purchase_form edd_purchase_415" method="post">


                                                                <div class="edd_purchase_submit_wrapper">
                                                                    <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="415" data-variable-price="no" data-price-mode="single" data-price="0.99"><span class="edd-add-to-cart-label">&#36;0.99</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                    <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="&#036;0.99" data-action="edd_add_to_cart" data-download-id="415" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                    <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                        <span class="edd-cart-added-alert" style="display: none;">
                                                                            <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                    </span>
                                                                </div>
                                                                <!--end .edd_purchase_submit_wrapper-->

                                                                <input type="hidden" name="download_id" value="415">
                                                                <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                            </form>
                                                            <!--end #edd_purchase_415-->
                                                        </div>
                                                    </div>
                                                    <a href="music/new-album/index.html" data-ajax title="New Album">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/m2-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m2-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m2.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/new-album/index.html" data-ajax title="New Album" class="text-ellipsis">New Album</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/miaow/index.html" rel="tag">Miaow</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-413" class="item post-413 download type-download status-publish has-post-thumbnail hentry download_category-hardcore download_artist-the-stark-palace odd edd-download edd-download-cat-hardcore">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="413" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <form id="edd_purchase_413" class="edd_download_purchase_form edd_purchase_413" method="post">


                                                                <div class="edd_purchase_submit_wrapper">
                                                                    <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="413" data-variable-price="no" data-price-mode="single" data-price="0.00"><span class="edd-add-to-cart-label">Free</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                    <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="Free" data-action="edd_add_to_cart" data-download-id="413" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                    <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                        <span class="edd-cart-added-alert" style="display: none;">
                                                                            <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                    </span>
                                                                </div>
                                                                <!--end .edd_purchase_submit_wrapper-->

                                                                <input type="hidden" name="download_id" value="413">
                                                                <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                            </form>
                                                            <!--end #edd_purchase_413-->
                                                        </div>
                                                    </div>
                                                    <a href="music/bundle/index.html" data-ajax title="Bundle">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/m3-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m3-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m3.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/bundle/index.html" data-ajax title="Bundle" class="text-ellipsis">Bundle</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/the-stark-palace/index.html" rel="tag">The Stark Palace</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-411" class="item post-411 download type-download status-publish has-post-thumbnail hentry download_category-blues download_artist-miaow even edd-download edd-download-cat-blues">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="411" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <form id="edd_purchase_411" class="edd_download_purchase_form edd_purchase_411" method="post">


                                                                <div class="edd_purchase_submit_wrapper">
                                                                    <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="411" data-variable-price="no" data-price-mode="single" data-price="0.00"><span class="edd-add-to-cart-label">Free</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                    <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="Free" data-action="edd_add_to_cart" data-download-id="411" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                    <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                        <span class="edd-cart-added-alert" style="display: none;">
                                                                            <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                    </span>
                                                                </div>
                                                                <!--end .edd_purchase_submit_wrapper-->

                                                                <input type="hidden" name="download_id" value="411">
                                                                <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                            </form>
                                                            <!--end #edd_purchase_411-->
                                                        </div>
                                                    </div>
                                                    <a href="music/free-album/index.html" data-ajax title="Free Album">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/b6-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b6-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b6.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/free-album/index.html" data-ajax title="Free Album" class="text-ellipsis">Free Album</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/miaow/index.html" rel="tag">Miaow</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-380" class="item post-380 download type-download status-publish has-post-thumbnail hentry download_category-acoustic download_tag-album download_artist-miaow odd edd-download edd-download-cat-acoustic edd-download-tag-album">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="380" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <form id="edd_purchase_380" class="edd_download_purchase_form edd_purchase_380" method="post">


                                                                <div class="edd_purchase_submit_wrapper">
                                                                    <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="380" data-variable-price="no" data-price-mode="single" data-price="0.29"><span class="edd-add-to-cart-label">&#36;0.29</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                    <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="&#036;0.29" data-action="edd_add_to_cart" data-download-id="380" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                    <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                        <span class="edd-cart-added-alert" style="display: none;">
                                                                            <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                    </span>
                                                                </div>
                                                                <!--end .edd_purchase_submit_wrapper-->

                                                                <input type="hidden" name="download_id" value="380">
                                                                <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                            </form>
                                                            <!--end #edd_purchase_380-->
                                                        </div>
                                                    </div>
                                                    <a href="music/miaow-2004/index.html" data-ajax title="Miaow 2004">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/m6-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m6-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m6.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/miaow-2004/index.html" data-ajax title="Miaow 2004" class="text-ellipsis">Miaow 2004</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/miaow/index.html" rel="tag">Miaow</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                            <article id="post-244" class="item post-244 download type-download status-publish has-post-thumbnail hentry download_category-acoustic download_category-ambient download_artist-the-stark-palace even edd-download edd-download-cat-acoustic edd-download-cat-ambient">
                                                <div class="pos-rlt">

                                                    <div class="item-overlay opacity r bg-black">
                                                        <div class="center text-center m-t-n">
                                                            <a href="javascript:;" data-id="244" class="play-me">
                                                                <i class="icon-control-play i-2x text"></i>
                                                                <i class="icon-control-pause i-2x text-active"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="item-overlay bottom">
                                                        <div class="bottom m-l-sm m-b-sm m-r-sm">
                                                            <div id="edd-modal-244" class="edd-popup">
                                                                <a href="#" class="edd-choose"></a>
                                                                <form id="edd_purchase_244" class="edd_download_purchase_form edd_purchase_244" method="post">

                                                                    <div class="edd_price_options edd_multi_mode">
                                                                        <ul>
                                                                            <li id="edd_price_option_244_regular">
                                                                                <label for="edd_price_option_244_1">
                                                                                    <input type="checkbox" checked='checked' name="edd_options[price_id][]" id="edd_price_option_244_1" class="edd_price_option_244" value="1" data-price="9.99" />&nbsp;<span class="edd_price_option_name">Regular</span><span class="edd_price_option_sep">&nbsp;&ndash;&nbsp;</span><span class="edd_price_option_price">&#36;9.99</span></label></li>
                                                                            <li id="edd_price_option_244_extended">
                                                                                <label for="edd_price_option_244_2">
                                                                                    <input type="checkbox" name="edd_options[price_id][]" id="edd_price_option_244_2" class="edd_price_option_244" value="2" data-price="19.99" />&nbsp;<span class="edd_price_option_name">Extended</span><span class="edd_price_option_sep">&nbsp;&ndash;&nbsp;</span><span class="edd_price_option_price">&#36;19.99</span></label></li>
                                                                        </ul>
                                                                    </div>
                                                                    <!--end .edd_price_options-->

                                                                    <div class="edd_purchase_submit_wrapper">
                                                                        <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="244" data-variable-price="yes" data-price-mode="multi" data-price="0"><span class="edd-add-to-cart-label">Purchase</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                        <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="Purchase" data-action="edd_add_to_cart" data-download-id="244" data-variable-price="yes" data-price-mode="multi" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                        <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                            <span class="edd-cart-added-alert" style="display: none;">
                                                                                <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                        </span>
                                                                    </div>
                                                                    <!--end .edd_purchase_submit_wrapper-->

                                                                    <input type="hidden" name="download_id" value="244">
                                                                    <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                                </form>
                                                                <!--end #edd_purchase_244-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="music/tsp-bundle/index.html" data-ajax title="TSP Bundle">
                                                        <img width="150" height="150" src="wp-content/uploads/2015/07/m7-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m7-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m7.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                    </a>
                                                </div>

                                                <div class="m-t-sm m-b-lg">
                                                    <div class="item-desc">
                                                        <a href="music/tsp-bundle/index.html" data-ajax title="TSP Bundle" class="text-ellipsis">TSP Bundle</a>
                                                        <div class="text-muted text-xs text-ellipsis">by <a href="artists/the-stark-palace/index.html" rel="tag">The Stark Palace</a></div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div id="music_post_widget-3" class="widget-1 widget-odd widget_music_post_widget widget">
                                            <h2 class="widget-title">Weekly Top Board</h2>
                                            <div class="sub-title">Popular songs over the wood</div>
                                            <div class="list-group list-group-lg">
                                                <div class="list-group-item">
                                                    <article id="post-1004" class="item hover m-b-none clearfix post-1004 download type-download status-publish has-post-thumbnail hentry download_category-country download_artist-trevorgoodrum odd edd-download edd-download-cat-country">
                                                        <div class="pos-rlt pull-left m-r">
                                                            <div class="item-overlay opacity r bg-black">
                                                                <div class="center text-center m-t-n-sm">
                                                                    <a href="javascript:;" data-id="1004" class="play-me i-lg">
                                                                        <i class="icon-control-play text"></i>
                                                                        <i class="icon-control-pause text-active"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <a href="music/country-girl-shake-it-for-me/index.html" data-ajax title="Country girl shake it for me" class="thumb-md">
                                                                <img width="150" height="150" src="wp-content/uploads/2015/07/m0-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m0-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m0.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                            </a>
                                                        </div>

                                                        <div class="clear">
                                                            <div class="pull-right hover-action">
                                                                <form id="edd_purchase_1004-2" class="edd_download_purchase_form edd_purchase_1004" method="post">


                                                                    <div class="edd_purchase_submit_wrapper">
                                                                        <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="1004" data-variable-price="no" data-price-mode="single" data-price="0.00"><span class="edd-add-to-cart-label">Free</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                        <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="Free" data-action="edd_add_to_cart" data-download-id="1004" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                        <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                            <span class="edd-cart-added-alert" style="display: none;">
                                                                                <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                        </span>
                                                                    </div>
                                                                    <!--end .edd_purchase_submit_wrapper-->

                                                                    <input type="hidden" name="download_id" value="1004">
                                                                    <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                                </form>
                                                                <!--end #edd_purchase_1004-2-->
                                                            </div>
                                                            <a href="music/country-girl-shake-it-for-me/index.html" data-ajax title="Country girl shake it for me" class="text-ellipsis font-bold">Country girl shake it for me</a>
                                                            <div class="text-muted text-sm text-ellipsis">by <a href="artists/trevorgoodrum/index.html" rel="tag">Trevorgoodrum</a></div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="list-group-item">
                                                    <article id="post-1425" class="item hover m-b-none clearfix post-1425 download type-download status-publish has-post-thumbnail hentry download_category-folk download_tag-radio even edd-download edd-download-cat-folk edd-download-tag-radio">
                                                        <div class="pos-rlt pull-left m-r">
                                                            <div class="item-overlay opacity r bg-black">
                                                                <div class="center text-center m-t-n-sm">
                                                                    <a href="javascript:;" data-id="1425" class="play-me i-lg">
                                                                        <i class="icon-control-play text"></i>
                                                                        <i class="icon-control-pause text-active"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <a href="music/shoutcast/index.html" data-ajax title="SHOUTcast" class="thumb-md">
                                                                <img width="150" height="150" src="wp-content/uploads/2015/07/photo-1422022098106-b3a9edc463af-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/photo-1422022098106-b3a9edc463af-150x150.jpeg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/photo-1422022098106-b3a9edc463af-350x350.jpeg 350w" sizes="(max-width: 150px) 100vw, 150px" />
                                                            </a>
                                                        </div>

                                                        <div class="clear">
                                                            <div class="pull-right hover-action">
                                                                <form id="edd_purchase_1425-2" class="edd_download_purchase_form edd_purchase_1425" method="post">


                                                                    <div class="edd_purchase_submit_wrapper">
                                                                        <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="1425" data-variable-price="no" data-price-mode="single" data-price="0.00"><span class="edd-add-to-cart-label">Free</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                        <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="Free" data-action="edd_add_to_cart" data-download-id="1425" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                        <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                            <span class="edd-cart-added-alert" style="display: none;">
                                                                                <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                        </span>
                                                                    </div>
                                                                    <!--end .edd_purchase_submit_wrapper-->

                                                                    <input type="hidden" name="download_id" value="1425">
                                                                    <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                                </form>
                                                                <!--end #edd_purchase_1425-2-->
                                                            </div>
                                                            <a href="music/shoutcast/index.html" data-ajax title="SHOUTcast" class="text-ellipsis font-bold">SHOUTcast</a>
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="list-group-item">
                                                    <article id="post-413" class="item hover m-b-none clearfix post-413 download type-download status-publish has-post-thumbnail hentry download_category-hardcore download_artist-the-stark-palace odd edd-download edd-download-cat-hardcore">
                                                        <div class="pos-rlt pull-left m-r">
                                                            <div class="item-overlay opacity r bg-black">
                                                                <div class="center text-center m-t-n-sm">
                                                                    <a href="javascript:;" data-id="413" class="play-me i-lg">
                                                                        <i class="icon-control-play text"></i>
                                                                        <i class="icon-control-pause text-active"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <a href="music/bundle/index.html" data-ajax title="Bundle" class="thumb-md">
                                                                <img width="150" height="150" src="wp-content/uploads/2015/07/m3-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m3-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m3.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                            </a>
                                                        </div>

                                                        <div class="clear">
                                                            <div class="pull-right hover-action">
                                                                <form id="edd_purchase_413-2" class="edd_download_purchase_form edd_purchase_413" method="post">


                                                                    <div class="edd_purchase_submit_wrapper">
                                                                        <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="413" data-variable-price="no" data-price-mode="single" data-price="0.00"><span class="edd-add-to-cart-label">Free</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                        <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="Free" data-action="edd_add_to_cart" data-download-id="413" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                        <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                            <span class="edd-cart-added-alert" style="display: none;">
                                                                                <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                        </span>
                                                                    </div>
                                                                    <!--end .edd_purchase_submit_wrapper-->

                                                                    <input type="hidden" name="download_id" value="413">
                                                                    <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                                </form>
                                                                <!--end #edd_purchase_413-2-->
                                                            </div>
                                                            <a href="music/bundle/index.html" data-ajax title="Bundle" class="text-ellipsis font-bold">Bundle</a>
                                                            <div class="text-muted text-sm text-ellipsis">by <a href="artists/the-stark-palace/index.html" rel="tag">The Stark Palace</a></div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="list-group-item">
                                                    <article id="post-411" class="item hover m-b-none clearfix post-411 download type-download status-publish has-post-thumbnail hentry download_category-blues download_artist-miaow even edd-download edd-download-cat-blues">
                                                        <div class="pos-rlt pull-left m-r">
                                                            <div class="item-overlay opacity r bg-black">
                                                                <div class="center text-center m-t-n-sm">
                                                                    <a href="javascript:;" data-id="411" class="play-me i-lg">
                                                                        <i class="icon-control-play text"></i>
                                                                        <i class="icon-control-pause text-active"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <a href="music/free-album/index.html" data-ajax title="Free Album" class="thumb-md">
                                                                <img width="150" height="150" src="wp-content/uploads/2015/07/b6-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b6-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b6.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                            </a>
                                                        </div>

                                                        <div class="clear">
                                                            <div class="pull-right hover-action">
                                                                <form id="edd_purchase_411-2" class="edd_download_purchase_form edd_purchase_411" method="post">


                                                                    <div class="edd_purchase_submit_wrapper">
                                                                        <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="411" data-variable-price="no" data-price-mode="single" data-price="0.00"><span class="edd-add-to-cart-label">Free</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                        <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="Free" data-action="edd_add_to_cart" data-download-id="411" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                        <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                            <span class="edd-cart-added-alert" style="display: none;">
                                                                                <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                        </span>
                                                                    </div>
                                                                    <!--end .edd_purchase_submit_wrapper-->

                                                                    <input type="hidden" name="download_id" value="411">
                                                                    <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                                </form>
                                                                <!--end #edd_purchase_411-2-->
                                                            </div>
                                                            <a href="music/free-album/index.html" data-ajax title="Free Album" class="text-ellipsis font-bold">Free Album</a>
                                                            <div class="text-muted text-sm text-ellipsis">by <a href="artists/miaow/index.html" rel="tag">Miaow</a></div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="list-group-item">
                                                    <article id="post-2180" class="item hover m-b-none clearfix post-2180 download type-download status-publish has-post-thumbnail hentry download_category-acoustic download_artist-trevorgoodrum odd edd-download edd-download-cat-acoustic">
                                                        <div class="pos-rlt pull-left m-r">
                                                            <div class="item-overlay opacity r bg-black">
                                                                <div class="center text-center m-t-n-sm">
                                                                    <a href="javascript:;" data-id="2180" class="play-me i-lg">
                                                                        <i class="icon-control-play text"></i>
                                                                        <i class="icon-control-pause text-active"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <a href="music/the-voice-inspiring-emotional-blind-auditions/index.html" data-ajax title="The Voice &#8211; Inspiring &#038; Emotional Blind Auditions" class="thumb-md">
                                                                <img width="150" height="150" src="wp-content/uploads/2015/07/b14-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b14-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b14.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                            </a>
                                                        </div>

                                                        <div class="clear">
                                                            <div class="pull-right hover-action">
                                                                <form id="edd_purchase_2180-2" class="edd_download_purchase_form edd_purchase_2180" method="post">


                                                                    <div class="edd_purchase_submit_wrapper">
                                                                        <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="2180" data-variable-price="no" data-price-mode="single" data-price="0.00"><span class="edd-add-to-cart-label">Free</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                        <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="Free" data-action="edd_add_to_cart" data-download-id="2180" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                        <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                            <span class="edd-cart-added-alert" style="display: none;">
                                                                                <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                        </span>
                                                                    </div>
                                                                    <!--end .edd_purchase_submit_wrapper-->

                                                                    <input type="hidden" name="download_id" value="2180">
                                                                    <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                                </form>
                                                                <!--end #edd_purchase_2180-2-->
                                                            </div>
                                                            <a href="music/the-voice-inspiring-emotional-blind-auditions/index.html" data-ajax title="The Voice &#8211; Inspiring &#038; Emotional Blind Auditions" class="text-ellipsis font-bold">The Voice &#8211; Inspiring &#038; Emotional Blind Auditions</a>
                                                            <div class="text-muted text-sm text-ellipsis">by <a href="artists/trevorgoodrum/index.html" rel="tag">Trevorgoodrum</a></div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="music_term_widget-1" class="widget-1 widget-odd widget_music_term_widget widget">
                                            <h3 class="widget-title">Popular Artist</h3>
                                            <h5 class="sub-title">Find your like</h5>
                                            <div class="list-group list-group-lg ">
                                                <div class="list-group-item clearfix">
                                                    <div class="pos-rlt pull-left m-r">
                                                        <a href="artists/miaow/index.html" class="thumb-sm">
                                                            <img src="wp-content/uploads/2015/07/m21-150x150.jpg" alt="Miaow" class="img-full rounded" />
                                                        </a>
                                                    </div>
                                                    <div class="">
                                                        <a href="artists/miaow/index.html" class="m-t-sm m-b-sm text-ellipsis">Miaow</a>
                                                    </div>
                                                </div>
                                                <div class="list-group-item clearfix">
                                                    <div class="pos-rlt pull-left m-r">
                                                        <a href="artists/the-stark-palace/index.html" class="thumb-sm">
                                                            <img src="wp-content/uploads/2015/07/b11-150x150.jpg" alt="The Stark Palace" class="img-full rounded" />
                                                        </a>
                                                    </div>
                                                    <div class="">
                                                        <a href="artists/the-stark-palace/index.html" class="m-t-sm m-b-sm text-ellipsis">The Stark Palace</a>
                                                    </div>
                                                </div>
                                                <div class="list-group-item clearfix">
                                                    <div class="pos-rlt pull-left m-r">
                                                        <a href="artists/bossr/index.html" class="thumb-sm">
                                                            <img src="wp-content/uploads/2015/07/m20-150x150.jpg" alt="Bossr" class="img-full rounded" />
                                                        </a>
                                                    </div>
                                                    <div class="">
                                                        <a href="artists/bossr/index.html" class="m-t-sm m-b-sm text-ellipsis">Bossr</a>
                                                    </div>
                                                </div>
                                                <div class="list-group-item clearfix">
                                                    <div class="pos-rlt pull-left m-r">
                                                        <a href="artists/trevorgoodrum/index.html" class="thumb-sm">
                                                            <img src="wp-content/themes/musiks/assets/images/default_300_300.jpg" alt="Trevorgoodrum" class="img-full rounded" />
                                                        </a>
                                                    </div>
                                                    <div class="">
                                                        <a href="artists/trevorgoodrum/index.html" class="m-t-sm m-b-sm text-ellipsis">Trevorgoodrum</a>
                                                    </div>
                                                </div>
                                                <div class="list-group-item clearfix">
                                                    <div class="pos-rlt pull-left m-r">
                                                        <a href="artists/crystal-guerrero/index.html" class="thumb-sm">
                                                            <img src="wp-content/uploads/2015/07/b0-150x150.jpg" alt="Crystal Guerrero" class="img-full rounded" />
                                                        </a>
                                                    </div>
                                                    <div class="">
                                                        <a href="artists/crystal-guerrero/index.html" class="m-t-sm m-b-sm text-ellipsis">Crystal Guerrero</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="text-2" class="widget-2 widget-even widget-alt widget_text widget">
                                            <div class="textwidget"><a href="#" class="btn  btn-sm btn-default btn-block">Invite your friends to join us</a></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="music_post_widget-4" class="widget-1 widget-odd widget_music_post_widget widget">
                                            <h3 class="widget-title">New Song</h3>
                                            <div class="sub-title">This week</div>
                                            <div class="list-group list-group-lg">
                                                <div class="list-group-item">
                                                    <article id="post-2183" class="item hover m-b-none clearfix post-2183 download type-download status-publish has-post-thumbnail hentry download_category-ambient download_tag-youtube download_artist-trevorgoodrum even edd-download edd-download-cat-ambient edd-download-tag-youtube">
                                                        <div class="pos-rlt pull-left m-r">
                                                            <div class="item-overlay opacity r bg-black">
                                                                <div class="center text-center m-t-n-sm">
                                                                    <a href="javascript:;" data-id="2183" class="play-me i-lg">
                                                                        <i class="icon-control-play text"></i>
                                                                        <i class="icon-control-pause text-active"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <a href="music/youtube/index.html" data-ajax title="Youtube" class="thumb-md">
                                                                <img width="150" height="150" src="wp-content/uploads/2015/07/photo-1413913619092-816734eed3a7-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/photo-1413913619092-816734eed3a7-150x150.jpeg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/photo-1413913619092-816734eed3a7-350x350.jpeg 350w" sizes="(max-width: 150px) 100vw, 150px" />
                                                            </a>
                                                        </div>

                                                        <div class="clear">
                                                            <div class="pull-right hover-action">
                                                                <form id="edd_purchase_2183" class="edd_download_purchase_form edd_purchase_2183" method="post">


                                                                    <div class="edd_purchase_submit_wrapper">
                                                                        <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="2183" data-variable-price="no" data-price-mode="single" data-price="0.00"><span class="edd-add-to-cart-label">Free</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                        <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="Free" data-action="edd_add_to_cart" data-download-id="2183" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                        <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                            <span class="edd-cart-added-alert" style="display: none;">
                                                                                <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                        </span>
                                                                    </div>
                                                                    <!--end .edd_purchase_submit_wrapper-->

                                                                    <input type="hidden" name="download_id" value="2183">
                                                                    <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                                </form>
                                                                <!--end #edd_purchase_2183-->
                                                            </div>
                                                            <a href="music/youtube/index.html" data-ajax title="Youtube" class="text-ellipsis font-bold">Youtube</a>
                                                            <div class="text-muted text-sm text-ellipsis">by <a href="artists/trevorgoodrum/index.html" rel="tag">Trevorgoodrum</a></div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="list-group-item">
                                                    <article id="post-2180" class="item hover m-b-none clearfix post-2180 download type-download status-publish has-post-thumbnail hentry download_category-acoustic download_artist-trevorgoodrum odd edd-download edd-download-cat-acoustic">
                                                        <div class="pos-rlt pull-left m-r">
                                                            <div class="item-overlay opacity r bg-black">
                                                                <div class="center text-center m-t-n-sm">
                                                                    <a href="javascript:;" data-id="2180" class="play-me i-lg">
                                                                        <i class="icon-control-play text"></i>
                                                                        <i class="icon-control-pause text-active"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <a href="music/the-voice-inspiring-emotional-blind-auditions/index.html" data-ajax title="The Voice &#8211; Inspiring &#038; Emotional Blind Auditions" class="thumb-md">
                                                                <img width="150" height="150" src="wp-content/uploads/2015/07/b14-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b14-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/b14.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                            </a>
                                                        </div>

                                                        <div class="clear">
                                                            <div class="pull-right hover-action">
                                                                <form id="edd_purchase_2180-3" class="edd_download_purchase_form edd_purchase_2180" method="post">


                                                                    <div class="edd_purchase_submit_wrapper">
                                                                        <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="2180" data-variable-price="no" data-price-mode="single" data-price="0.00"><span class="edd-add-to-cart-label">Free</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                        <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="Free" data-action="edd_add_to_cart" data-download-id="2180" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                        <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                            <span class="edd-cart-added-alert" style="display: none;">
                                                                                <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                        </span>
                                                                    </div>
                                                                    <!--end .edd_purchase_submit_wrapper-->

                                                                    <input type="hidden" name="download_id" value="2180">
                                                                    <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                                </form>
                                                                <!--end #edd_purchase_2180-3-->
                                                            </div>
                                                            <a href="music/the-voice-inspiring-emotional-blind-auditions/index.html" data-ajax title="The Voice &#8211; Inspiring &#038; Emotional Blind Auditions" class="text-ellipsis font-bold">The Voice &#8211; Inspiring &#038; Emotional Blind Auditions</a>
                                                            <div class="text-muted text-sm text-ellipsis">by <a href="artists/trevorgoodrum/index.html" rel="tag">Trevorgoodrum</a></div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="music_post_widget-5" class="widget-2 widget-even widget-alt widget_music_post_widget widget">
                                            <div class="sub-title">Popular Albums</div>
                                            <div class="list-group list-group-lg">
                                                <div class="list-group-item">
                                                    <article id="post-417" class="item hover m-b-none clearfix post-417 download type-download status-publish has-post-thumbnail hentry download_category-acoustic download_artist-bossr even edd-download edd-download-cat-acoustic">
                                                        <div class="pos-rlt pull-left m-r">
                                                            <div class="item-overlay opacity r bg-black">
                                                                <div class="center text-center m-t-n-sm">
                                                                    <a href="javascript:;" data-id="417" class="play-me i-lg">
                                                                        <i class="icon-control-play text"></i>
                                                                        <i class="icon-control-pause text-active"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <a href="music/top-board-june/index.html" data-ajax title="Top Board &#8211; June" class="thumb-md">
                                                                <img width="150" height="150" src="wp-content/uploads/2015/07/m13-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m13-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m13.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                            </a>
                                                        </div>

                                                        <div class="clear">
                                                            <a href="music/top-board-june/index.html" data-ajax title="Top Board &#8211; June" class="text-ellipsis font-bold">Top Board &#8211; June</a>
                                                            <div class="text-muted text-sm text-ellipsis">by <a href="artists/bossr/index.html" rel="tag">Bossr</a></div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <div class="list-group-item">
                                                    <article id="post-415" class="item hover m-b-none clearfix post-415 download type-download status-publish has-post-thumbnail hentry download_category-country download_artist-miaow odd edd-download edd-download-cat-country">
                                                        <div class="pos-rlt pull-left m-r">
                                                            <div class="item-overlay opacity r bg-black">
                                                                <div class="center text-center m-t-n-sm">
                                                                    <a href="javascript:;" data-id="415" class="play-me i-lg">
                                                                        <i class="icon-control-play text"></i>
                                                                        <i class="icon-control-pause text-active"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <a href="music/new-album/index.html" data-ajax title="New Album" class="thumb-md">
                                                                <img width="150" height="150" src="wp-content/uploads/2015/07/m2-150x150.jpg" class="r img-full wp-post-image" alt="" src="http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m2-150x150.jpg 150w, http://flatfull.com/wp/musik/wp-content/uploads/2015/07/m2.jpg 256w" sizes="(max-width: 150px) 100vw, 150px" />
                                                            </a>
                                                        </div>

                                                        <div class="clear">
                                                            <div class="pull-right hover-action">
                                                                <form id="edd_purchase_415-2" class="edd_download_purchase_form edd_purchase_415" method="post">


                                                                    <div class="edd_purchase_submit_wrapper">
                                                                        <a href="#" class="edd-add-to-cart btn btn-xs btn-danger blue edd-submit" data-action="edd_add_to_cart" data-download-id="415" data-variable-price="no" data-price-mode="single" data-price="0.99"><span class="edd-add-to-cart-label">&#36;0.99</span> <span class="edd-loading"><i class="edd-icon-spinner edd-icon-spin"></i></span></a>
                                                                        <input type="submit" class="edd-add-to-cart edd-no-js btn btn-xs btn-danger blue edd-submit" name="edd_purchase_download" value="&#036;0.99" data-action="edd_add_to_cart" data-download-id="415" data-variable-price="no" data-price-mode="single" /><a href="checkout/index.html" class="edd_go_to_checkout btn btn-xs btn-danger blue edd-submit" style="display: none;">Checkout</a>
                                                                        <span class="edd-cart-ajax-alert" aria-live="assertive">
                                                                            <span class="edd-cart-added-alert" style="display: none;">
                                                                                <i class="edd-icon-ok" aria-hidden="true"></i>Added to cart					</span>
                                                                        </span>
                                                                    </div>
                                                                    <!--end .edd_purchase_submit_wrapper-->

                                                                    <input type="hidden" name="download_id" value="415">
                                                                    <input type="hidden" name="edd_action" class="edd_action_input" value="add_to_cart">
                                                                </form>
                                                                <!--end #edd_purchase_415-2-->
                                                            </div>
                                                            <a href="music/new-album/index.html" data-ajax title="New Album" class="text-ellipsis font-bold">New Album</a>
                                                            <div class="text-muted text-sm text-ellipsis">by <a href="artists/miaow/index.html" rel="tag">Miaow</a></div>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                            </section>
                        </section>
                    </section>
                    <aside class="aside aside-md bg-light dk">
                        <section class="vbox">
                            <section class="scrollable wrapper hover" id="home-sidebar">
                                <div id="music_term_widget-2" class="widget-1 widget-odd widget_music_term_widget widget">
                                    <h4 class="widget-title">Top artists </h4>
                                    <div class="list-group list-group-lg ">
                                        <div class="list-group-item clearfix">
                                            <div class="pos-rlt pull-left m-r">
                                                <a href="artists/miaow/index.html" class="thumb-sm">
                                                    <img src="wp-content/uploads/2015/07/m21-150x150.jpg" alt="Miaow" class="img-full rounded" />
                                                </a>
                                            </div>
                                            <div class="">
                                                <a href="artists/miaow/index.html" class="m-t-sm m-b-sm text-ellipsis">Miaow</a>
                                            </div>
                                        </div>
                                        <div class="list-group-item clearfix">
                                            <div class="pos-rlt pull-left m-r">
                                                <a href="artists/the-stark-palace/index.html" class="thumb-sm">
                                                    <img src="wp-content/uploads/2015/07/b11-150x150.jpg" alt="The Stark Palace" class="img-full rounded" />
                                                </a>
                                            </div>
                                            <div class="">
                                                <a href="artists/the-stark-palace/index.html" class="m-t-sm m-b-sm text-ellipsis">The Stark Palace</a>
                                            </div>
                                        </div>
                                        <div class="list-group-item clearfix">
                                            <div class="pos-rlt pull-left m-r">
                                                <a href="artists/bossr/index.html" class="thumb-sm">
                                                    <img src="wp-content/uploads/2015/07/m20-150x150.jpg" alt="Bossr" class="img-full rounded" />
                                                </a>
                                            </div>
                                            <div class="">
                                                <a href="artists/bossr/index.html" class="m-t-sm m-b-sm text-ellipsis">Bossr</a>
                                            </div>
                                        </div>
                                        <div class="list-group-item clearfix">
                                            <div class="pos-rlt pull-left m-r">
                                                <a href="artists/trevorgoodrum/index.html" class="thumb-sm">
                                                    <img src="wp-content/themes/musiks/assets/images/default_300_300.jpg" alt="Trevorgoodrum" class="img-full rounded" />
                                                </a>
                                            </div>
                                            <div class="">
                                                <a href="artists/trevorgoodrum/index.html" class="m-t-sm m-b-sm text-ellipsis">Trevorgoodrum</a>
                                            </div>
                                        </div>
                                        <div class="list-group-item clearfix">
                                            <div class="pos-rlt pull-left m-r">
                                                <a href="artists/crystal-guerrero/index.html" class="thumb-sm">
                                                    <img src="wp-content/uploads/2015/07/b0-150x150.jpg" alt="Crystal Guerrero" class="img-full rounded" />
                                                </a>
                                            </div>
                                            <div class="">
                                                <a href="artists/crystal-guerrero/index.html" class="m-t-sm m-b-sm text-ellipsis">Crystal Guerrero</a>
                                            </div>
                                        </div>
                                        <div class="list-group-item clearfix">
                                            <div class="pos-rlt pull-left m-r">
                                                <a href="artists/james-garcia/index.html" class="thumb-sm">
                                                    <img src="wp-content/uploads/2015/07/b1-150x150.jpg" alt="James Garcia" class="img-full rounded" />
                                                </a>
                                            </div>
                                            <div class="">
                                                <a href="artists/james-garcia/index.html" class="m-t-sm m-b-sm text-ellipsis">James Garcia</a>
                                            </div>
                                        </div>
                                        <div class="list-group-item clearfix">
                                            <div class="pos-rlt pull-left m-r">
                                                <a href="artists/comma/index.html" class="thumb-sm">
                                                    <img src="wp-content/uploads/2015/07/b10-150x150.jpg" alt="Jean Schneider" class="img-full rounded" />
                                                </a>
                                            </div>
                                            <div class="">
                                                <a href="artists/comma/index.html" class="m-t-sm m-b-sm text-ellipsis">Jean Schneider</a>
                                            </div>
                                        </div>
                                        <div class="list-group-item clearfix">
                                            <div class="pos-rlt pull-left m-r">
                                                <a href="artists/jeremy-scott/index.html" class="thumb-sm">
                                                    <img src="wp-content/uploads/2015/07/b9-150x150.jpg" alt="Jeremy Scott" class="img-full rounded" />
                                                </a>
                                            </div>
                                            <div class="">
                                                <a href="artists/jeremy-scott/index.html" class="m-t-sm m-b-sm text-ellipsis">Jeremy Scott</a>
                                            </div>
                                        </div>
                                        <div class="list-group-item clearfix">
                                            <div class="pos-rlt pull-left m-r">
                                                <a href="artists/joe-holmes/index.html" class="thumb-sm">
                                                    <img src="wp-content/uploads/2015/07/b12-150x150.jpg" alt="Joe Holmes" class="img-full rounded" />
                                                </a>
                                            </div>
                                            <div class="">
                                                <a href="artists/joe-holmes/index.html" class="m-t-sm m-b-sm text-ellipsis">Joe Holmes</a>
                                            </div>
                                        </div>
                                        <div class="list-group-item clearfix">
                                            <div class="pos-rlt pull-left m-r">
                                                <a href="artists/judith-garcia/index.html" class="thumb-sm">
                                                    <img src="wp-content/uploads/2015/07/m18-150x150.jpg" alt="Judith Garcia" class="img-full rounded" />
                                                </a>
                                            </div>
                                            <div class="">
                                                <a href="artists/judith-garcia/index.html" class="m-t-sm m-b-sm text-ellipsis">Judith Garcia</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="music_term_widget-3" class="widget-2 widget-even widget-alt widget_music_term_widget widget">
                                    <h5 class="sub-title">Popular Genres</h5>
                                    <ul class="m-t-n-xxs list-inline">
                                        <li><a class="" href="genre/acoustic/index.html">Acoustic</a></li>
                                        <li><a class="" href="genre/ambient/index.html">Ambient</a></li>
                                        <li><a class="" href="genre/blues/index.html">Blues</a></li>
                                        <li><a class="" href="genre/country/index.html">Country</a></li>
                                        <li><a class="" href="genre/folk/index.html">Folk</a></li>
                                        <li><a class="" href="genre/hardcore/index.html">Hardcore</a></li>
                                    </ul>
                                </div>
                            </section>
                        </section>
                    </aside>
                </section>
            </section>
        </div>
        <script>
            $(document).ready(setInterval(function () {
                var isMobile = false; //initiate as false
                // device detection
                var isMobileH = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
                    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4));
                if (isMobileH) {
                    isMobile = true;
                    $(".ourApp").css("display", "none");
                    $(".ourLogin").removeClass("col-sm-6");
                    $(".ourLogin").addClass("col-sm-12");


                }
                if (!isMobileH) {
                    $(".ourApp").css("display", "block");
                    $(".ourLogin").removeClass("col-sm-12");
                    $(".ourLogin").addClass("col-sm-6");

                }
                var ourLoginH = $(".ourLogin").height();
                var ourAppH = $(".ourApp").height();

                if (ourLoginH > ourAppH) {
                    $(".ourApp .wrapper-md").css("min-height", ourLoginH + "px");
                    $(".ourLogin .wrapper-md").css("min-height", ourLoginH + "px");
                    //alert("app height : " + ourAppH);
                    //alert("login height : " + ourLoginH);
                }
                if (ourAppH > ourLoginH) {
                    $(".ourLogin .wrapper-md").css("min-height", ourAppH + "px");
                    $(".ourApp .wrapper-md").css("min-height", ourAppH + "px");
                }

            }, 3));
        </script>
    </form>
</body>
</html>
